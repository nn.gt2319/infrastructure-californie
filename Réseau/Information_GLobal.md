# Information générale

* Notre agence dispose d’un serveur Proxmox nommé « pve-Cadec ». Ce dernier est nécessaire pour l’hébergement des serveurs Windows et divers services
    * Caractéristiques du serveur Proxmox :
        - Modèle : DELL 1950 format rack
        - Nom : PveCadec.sitec.fr
        - Configuration IP : 192.168.3.170/24 (VLAN ADMINISTRATION) Passerelle : 192.168.3.254
        - DNS : 8.8.8.8 - 8.8.4.4
* Caractéristique matérieles de notre serveur Proxmox :
    * Processeur :
        - Intel(R) Celeron(R) CPU G3900 @ 2.80GHz
        - Un seul processeur
        - 1 thread par cœur
        - 2 cœurs par socket
    * Mémoire cache :
        - L1d 64 KiB (2 instances) 
        - L1i 64 KiB (2 instances)
        - L2 512 KiB (2 instances)
        - L3 2 MiB (1 instance)
    * Disque 1 : 
        - 500 GB TOSHIBA_DT01ACA050
    * Disque 2 :
        - 1.00 TB WDC_WD1001FAES-60Z2A0
    * RAM :
        - 16 GO
    * Carte graphique :
        - Integré 
    * Nombres de cœurs disponibles : 
        - 2