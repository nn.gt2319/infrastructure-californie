# Déploiement du serveur SNMP

## Informations 

* L’objectif est d’intégrer un superviseur en ligne de commande sous Linux pour superviser certains éléments de notre agence via SNMP.
* Pour cela, une machine virtuelle a été créée mais non configurée :
    * servSNMPcalifornie : 192.168.3.28/24 (VM 28 sur pveSTORM2)

## Configuration 

* Installation du paquets SNMP :
    * apt install snmp
    * apt install snmp-mibs-downloader
* Il faudra aussi que vous fassiez le nécessaire sur votre serveur DHCP :
    * apt-get install snmpd
    * nano /etc/snmp/snmpd.conf
        - _rocommunity_ _public_
    * service snmpd restart



