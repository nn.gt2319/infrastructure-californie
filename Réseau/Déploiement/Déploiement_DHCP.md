# Déploiement du serveur DHCP 

## Informations 

* Notre agence dispose de plusieurs postes de travail répartis dans deux VLANs : Production et SITEC. Il est nécessaire de mettre en place une automatisation des configurations IP des postes de travail en configurant un serveur DHCP qui sera hébergé dans le VLAN SITEC.
* Voici les informations de la machine virtuelle crée pour le service DHCP :
    * servDHCPcalifornie : 192.168.3.30/2 (VM 30 sur pveSTORM2)

## Prérequis 

* Il faut activer le relais DHCP sur le Firewall Stormshield pour permettre la liasion entre notre serveur et notre boitier. Pour ce faire, il faut se rendre dans le munu _Configuration_ -> _Réseau_ -> _DHCP_.
* Il faut créer un objet _servDHCPcalifornie_.
* Installer le service NTP sur le serveur DHCP.

## Configuration 

* Sur une Debian 12 Bookworm (dernière version stable actuelle), l'installation d'un serveur DHCP se fait à partir du paquet “isc-dhcp-server”.

    ```apt install isc-dhcp-server```
* L'installation de ce paquet va créer notamment les deux fichiers suivants : 
    *   /etc/dhcp/dhcpd.conf : il s'agit du fichier de configuration du serveur. C'est dans ce fichier que l'administrateur va indiquer les différentes plages d'adresses nécessaires, les paramètres à distribuer, etc... ;

* Exemple de notre fichier conf :
```
option domain-name "cadec.fr";
option domain-name-servers 192.168.3.100
default-lease-time 21600;

subnet 192.168.3.0 netmask 255.255.255.0
range 192.168.3.31 192.168.3.99
option routers 192.168.3
```


# Mise en place d'un dispositif de haute disponibilité

## Informations 

* Voici les informations de la deuxième machine virtuelle crée pour le service DHCP :
    * HDservDHCP  : 192.168.3.29/24 (VM 29 sur pveSTORM2)
* Installer le service DHCP sur cette machine puis stopper ce service :
    * apt install isc-dhcp-server 
    * systemctl stop isc-dhcp-server
* Installer le service NTP sur le deuxième serveur DHCP pour les synchroniser
* Appliquer la même configuration /etc/dhcp/dhcpd.conf pour le deuxième serveur.

## Configuration de la haut disponibilité

* Sur le serveur primaire (192.168.3.30) se rendre dans /etc/dhcp/dhcpd.conf :
```
failvoer peer "dhcp-failover"{
    primary;
    address 192.168.3.30
    port 647;
    peer address 192.168.3.29
    peer port 847;
    max-response-delay 60;
    max-unacked-updates 10;
    load balance max secondes 3;
    mclt 3600;
    split 128;
}
```
* Sur le serveur secondaire se rendre dans /etc/dhcp/dhcpd.conf :
```
failvoer peer "dhcp-failover"{
    secondary;
    address 192.168.3.29
    port 847;
    peer address 192.168.3.30
    peer port 647;
    max-response-delay 60;
    max-unacked-updates 10;
    load balance max secondes 3;
```


    