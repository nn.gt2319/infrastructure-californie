# Configuration d'un DNS Maître et Esclave sous Debian 12

Cette documentation explique comment configurer un serveur DNS maître et un serveur DNS esclave sous Debian 12, en utilisant **Bind9**. Le serveur maître aura l'adresse IP **192.168.3.100** et le serveur esclave l'adresse IP **192.168.3.101**.

## **Prérequis**
- Deux machines sous Debian 12 avec des adresses IP statiques configurées :
  - Maître : 192.168.3.100
  - Esclave : 192.168.3.101
- Droits sudo sur les deux machines.
- Bind9 installé sur les deux serveurs.

### **Installation de Bind9**
Sur chaque serveur, exécutez :
```bash
sudo apt update
sudo apt install bind9 bind9-utils
```

---

## **Configuration du DNS Maître (192.168.3.100)**

### **1. Configuration de la zone DNS**
1. Éditez le fichier de configuration principal de Bind9 :
   ```bash
   sudo nano /etc/bind/named.conf.local
   ```
2. Ajoutez la définition de la zone maître :
   ```
   zone "cadec.fr" {
       type master;
       file "/etc/bind/db.cadec.fr";
       allow-transfer { 192.168.3.101; };
   };
   ```

### **2. Création du fichier de zone**
1. Créez le fichier de zone :
   ```bash
   sudo nano /etc/bind/db.cadec.fr
   ```
2. Ajoutez le contenu suivant :
   ```
   ; Fichier de zone pour cadec.fr
   $TTL 604800
   @       IN      SOA     ns1.cadec.fr. admin.cadec.fr. (
                           2025012301 ; Numéro de série
                           604800     ; Refresh
                           86400      ; Retry
                           2419200    ; Expire
                           604800 )   ; Negative Cache TTL

   ; Serveurs de noms
   @       IN      NS      ns1.cadec.fr.
   @       IN      NS      ns2.cadec.fr.

   ; Adresses des serveurs
   ns1     IN      A       192.168.3.100
   ns2     IN      A       192.168.3.101

   ; Enregistrements exemple
   www     IN      A       192.168.3.110
   mail    IN      A       192.168.3.111
   ```

### **3. Redémarrage du service**
Vérifiez la configuration et redémarrez Bind9 :
```bash
sudo named-checkconf
sudo named-checkzone example.com /etc/bind/db.cadec.fr
sudo systemctl restart bind9
```

---

## **Configuration du DNS Esclave (192.168.3.101)**

### **1. Configuration de la zone DNS**
1. Éditez le fichier de configuration principal de Bind9 :
   ```bash
   sudo nano /etc/bind/named.conf.local
   ```
2. Ajoutez la définition de la zone esclave :
   ```
   zone "example.com" {
       type slave;
       file "/var/cache/bind/db.cadec.fr";
       masters { 192.168.3.100; };
   };
   ```

### **2. Redémarrage du service**
Vérifiez la configuration et redémarrez Bind9 :
```bash
sudo named-checkconf
sudo systemctl restart bind9
```

---

## **Tests et Validation**

### **1. Vérification du transfert de zone**
Sur le serveur esclave, vérifiez que la zone a été transférée :
```bash
sudo tail -f /var/log/syslog
```
Recherchez des messages confirmant le transfert de la zone depuis le maître.

### **2. Test de résolution DNS**
Depuis une machine cliente ou l'un des serveurs :
```bash
dig @192.168.3.100 www.cadec.fr
```
```bash
dig @192.168.3.101 www.cadec.fr
```
Assurez-vous que les deux serveurs résolvent correctement le nom.

### **3. Simulation de panne**
Arrêtez Bind9 sur le serveur maître pour tester la redondance :
```bash
sudo systemctl stop bind9
```
Ensuite, testez la résolution sur le serveur esclave :
```bash
dig @192.168.3.101 www.cadedc.fr

---

