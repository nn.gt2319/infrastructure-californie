# Déploiement du pare-feu Stormshield

## Informations
* Une prise murale et un commutateur « SW-ARUBA » a été configurée pour pouvoir configurer des terminaux sur ces réseaux et permettre notamment un accès à l’interface web de configuration du FW associé.
* Notre boitier a reçu une pré-configuration de la part de notre administrateur réseau. Il faut verifier si  certains éléments ont été bien configuré pour notre agence.
* Pour se connecter à l'interface graphique, il faut taper dans sa barre de recherche https://192.168.3.190
* Après reflexion avec notre administrateur réseau. Le réseau IN sera le suivant :
    * 192.168.3.0/24 Pour PRODUCTION
    

## Configuration

* Une fois connecté à l'interface, il faut se rendre danss _Configuration_ -> _Configuration générale_ pour changer le nom en FW-CADEC.
* Pour voir la configuration réseau faite par notre administrateur réseau, il faut se rendre dans _Réseau_ -> _Interfaces_.
* Il faut activer le SSH en cas de perte d'accés de l'interface graphque. Pour ce faire, aller dans _Système_ -> _Configuration_ -> _Administration_.
* Vérifier les passerelles des réseaux dans _Objets_ -> _Réseau_.

## Configuration du VPN IPsec sur le FW-CADEC

