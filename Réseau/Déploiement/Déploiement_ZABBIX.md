# Déploiement du serveur ZABBIX

## Informations 

* IP : 192.168.3.27/254(VLAN PRODUCTION)
* Nom : ServZabbixCadec

---

## **Prérequis**

1. Une machine sous Debian 12 avec une adresse IP statique.
2. Accès root ou sudo.
3. Un serveur de base de données compatible (MySQL/MariaDB ou PostgreSQL).

---

## **1. Mise à jour du système**

Avant de commencer, mettez à jour les paquets du système :
```bash
sudo apt update && sudo apt upgrade -y
```

---

## **2. Installation du serveur de base de données**

Pour cet exemple, nous utilisons **MariaDB** comme base de données :

1. Installez MariaDB :
   ```bash
   sudo apt install mariadb-server -y
   ```

2. Configurez la sécurité de MariaDB :
   ```bash
   sudo mysql_secure_installation
   ```
   Suivez les instructions pour sécuriser l'installation (mot de passe root, suppression des bases inutiles, etc.).

3. Créez une base de données pour Zabbix :
   ```bash
   sudo mysql -u root -p
   ```
   Dans le shell MySQL :
   ```sql
   CREATE DATABASE zabbix CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
   CREATE USER 'zabbix'@'localhost' IDENTIFIED BY 'MotDePasseFort';
   GRANT ALL PRIVILEGES ON zabbix.* TO 'zabbix'@'localhost';
   FLUSH PRIVILEGES;
   EXIT;
   ```

---

## **3. Ajout du dépôt Zabbix**

1. Téléchargez et installez le dépôt officiel de Zabbix pour Debian 12 :
   ```bash
   wget https://repo.zabbix.com/zabbix/6.0/debian/pool/main/z/zabbix-release/zabbix-release_6.0-4%2Bdebian12_all.deb
   sudo dpkg -i zabbix-release_6.0-4+debian12_all.deb
   sudo apt update
   ```

---

## **4. Installation de Zabbix Server, Frontend et Agent**

1. Installez les paquets Zabbix et Apache :
   ```bash
   sudo apt install zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-agent -y
   ```

2. Importez le schéma initial de la base de données Zabbix :
   ```bash
   zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -u zabbix -p zabbix
   ```

---

## **5. Configuration de Zabbix Server**

1. Éditez le fichier de configuration du serveur Zabbix :
   ```bash
   sudo nano /etc/zabbix/zabbix_server.conf
   ```
2. Modifiez les paramètres suivants :
   ```
   DBHost=localhost
   DBName=zabbix
   DBUser=zabbix
   DBPassword=MotDePasseFort
   ```
3. Sauvegardez et fermez le fichier.

---

## **6. Configuration de PHP pour Zabbix**

1. Éditez le fichier de configuration PHP pour Zabbix :
   ```bash
   sudo nano /etc/zabbix/apache.conf
   ```
2. Assurez-vous que les paramètres suivants sont corrects :
   ```
   php_value date.timezone Europe/Paris
   ```
3. Redémarrez Apache :
   ```bash
   sudo systemctl restart apache2
   ```

---

## **7. Démarrage des services Zabbix**

1. Démarrez les services Zabbix et configurez-les pour qu'ils se lancent au démarrage :
   ```bash
   sudo systemctl restart zabbix-server zabbix-agent
   sudo systemctl enable zabbix-server zabbix-agent
   ```

---

## **8. Accès au frontend Zabbix**

1. Ouvrez un navigateur web et accédez à :
   ```
   http://<IP_de_votre_serveur>/zabbix
   ```
2. Suivez les étapes de l'assistant d'installation :
   - Base de données :
     - Hôte : localhost
     - Nom : zabbix
     - Utilisateur : zabbix
     - Mot de passe : MotDePasseFort
   - Validez les paramètres et complétez l'installation.

3. Connectez-vous avec les identifiants par défaut :
   - **Utilisateur** : Admin
   - **Mot de passe** : zabbix

---

## **9. Tests et validation**

1. Assurez-vous que le serveur et l'agent fonctionnent correctement :
   ```bash
   sudo systemctl status zabbix-server zabbix-agent
   ```
2. Ajoutez des hôtes à superviser depuis l'interface web Zabbix.

---

## **Conclusion**
Vous avez maintenant un serveur Zabbix pleinement fonctionnel sur Debian 12. Il peut être utilisé pour superviser vos infrastructures informatiques. Pensez à configurer des agents sur les machines à surveiller et à personnaliser les métriques en fonction de vos besoins.

## Finalisation 
* Durant la finalisation de votre configuration sur le serveur web de zabbix vous allez rencontrer une erreur sur la page du fuseau d'horaires vous ne pourrez pas passer à la page suivante car votre firewall Stormshield bloque par défaut.

* Pour finir l'installation sur l'interface web, il faut : 
    * Se rendre dans votre Firewall de votre agence puis dans _Configuration_ -> _Protections Applicatives_ -> _Applications et protections_
    * Taper ceci dans la barre de recherche (buffer overflow in HTML)![Alt text](image-9.png)
    * Passer l'alerte en _autoriser_ pour finir la configuration de votre serveur.
