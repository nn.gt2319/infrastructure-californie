# Déploiement du commutateur Aruba

## Informations

* Le VLAN de management par défaut (VLAN 1) sera utilisé pour l’accès à l’interface d’administration du commutateur. Ce réseau sera 10.0.3.0/24 et l’adresse de management sera 10.0.3.254.
* Le compte utilisateur permettant l’administration du commutateur aura les caractéristiques suivantes :
    * identifiant : adminsw
    * mot de passe : enable$Admin0
* Le nom d’hôte du commutateur sera de la forme SW-LAN-CALIFORNIE. ➔ Les VLANs seront définis ainsi :
    - VLAN 10 -> PRODUCTION -> Ports 1 à 2
    - VLAN 20 -> COLLABORATEURS -> Ports 3 à 6
    - VLAN 1 -> MANAGEMENT -> Port 7
* Le port d’interconnexion avec FW-CALIFORNIE sera le 8.

## Configuration

* Une fois connecté au commutateur, il faut se cliquer sur les trois traits en haut à droite plus aller dans _Dashboard_ pou rpouvoir changer le nom de ce dernier.
* Pour le configuration IP, il faut se rendre dans _Setup Network_ -> _Get Connected_ puis mettre l'ip désirée.
* La création des VLANs se fait dans _VLAN_ -> _VLAN Configuration_. 
* Il faut "tagger" le port 8 qui permet l'interco avec le boitier SN.

